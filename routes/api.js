var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');

/* GET home page. */
router.get('/:nomeArquivo/:content', function(req, res, next) {
  try {
    const nomeArquivo = `${req.param('nomeArquivo')}.txt`
    const content = req.param('content');
    const binary = new Uint8Array(Buffer.from(content));

    fs.writeFile(path.join(__dirname, '../files', nomeArquivo), binary, err => {
      if (err) res.status(500).send(JSON.stringify(err));
      // console.log('The file has been saved!');
      else res.status(200).send({nomeArquivo, content});
    });    
  } catch (e) {
    res.status(500).send(JSON.stringify(e));
  }
});

/* GET home page. */
router.get('/', function(req, res, next) {

  res.status(200).send();
});

module.exports = router;